package cn.uncode.springcloud.starter.bus.event;


import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;

import cn.uncode.springcloud.utils.obj.SpringUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * <一句话功能简述> <功能详细描述>
 *
 * @see: [相关类/方法]（可选）
 * @since [产品/模块版本] （可选）
 */
@Slf4j
public class BaseEvent<T> extends ApplicationEvent {

    /**
	 * 
	 */
	private static final long serialVersionUID = 187180320877892048L;

	public BaseEvent(T source) {
        super(source);

    }

    public void fire() {
        ApplicationContext context = SpringUtil.getContext();
        if (null != context) {
            context.publishEvent(this);
            log.info("发布本地事件：{}", this);
        }
    }
}
