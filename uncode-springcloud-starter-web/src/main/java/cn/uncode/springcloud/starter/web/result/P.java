package cn.uncode.springcloud.starter.web.result;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.uncode.springcloud.utils.obj.BeanUtil;
import cn.uncode.springcloud.utils.obj.ObjectUtil;
import lombok.Data;

@Data
public class P<T> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    public static final  String DATA = "data";
    public static final  String PAGE = "page";
	
	 /**
     * 当前页码
     */
    private int pageIndex;

    /**
     * 单页面记录总数
     */
    private int pageSize;

    /**
     * 总页数
     */
    private int pageTotal;

    /**
     * 总记录数
     */
    private int recordTotal;
    
    
    
    private T data;

    
//    public static <T> P<T> valueOf(Page<T> page) {
//    	P<T> p = new P<T>();
//    	if(null != page) {
//    		p.setData(page.getResult());
//    		p.setPageIndex(page.getPageNum());
//    		p.setPageSize(page.getPageSize());
//    		p.setPageTotal(page.getPages());
//    		p.setRecordTotal((int)page.getTotal());
//    	}
//    	return p;
//    }
    
    
    public static <T> P<List<T>> valueOfDAL(Map<String, Object> resultMap, Class<T> clazz) {
    	P<List<T>> p = new P<>();
    	if(null != resultMap) {
    		Map<String, Object> pageMap = (Map<String, Object>) resultMap.get(PAGE);
    		if(null != pageMap) {
    			Object index = pageMap.get("pageIndex");
        		if(ObjectUtil.isNotEmpty(index)) {
        			p.setPageIndex(Integer.valueOf(String.valueOf(index)));
        		}
        		Object size = pageMap.get("pageSize");
        		if(ObjectUtil.isNotEmpty(size)) {
        			p.setPageSize(Integer.valueOf(String.valueOf(size)));
        		}
        		Object pTotal = pageMap.get("pageCount");
        		if(ObjectUtil.isNotEmpty(pTotal)) {
        			p.setPageTotal(Integer.valueOf(String.valueOf(pTotal)));
        		}
        		Object rTotal = pageMap.get("recordTotal");
        		if(ObjectUtil.isNotEmpty(rTotal)) {
        			p.setRecordTotal(Integer.valueOf(String.valueOf(rTotal)));
        		}
    		}
    		List<Map<String, Object>> list = (List<Map<String, Object>>) resultMap.get(DATA);
    		if(null != list) {
    			p.setData(mapListToObjList(list, clazz));
    		}
    	}
    	return p;
    }
    
    private static <T> List<T> mapListToObjList(List<Map<String, Object>> resultList, Class<T> beanClass) {
    	if(null != resultList && resultList.size() > 0){
    		List<T> rtList = new ArrayList<T>();
    		for(Map<String, Object> itemMap:resultList){
				rtList.add(BeanUtil.toBean(itemMap, beanClass));
			}
            return rtList;
    	}
    	return null;
    }
    
    public Map<String, Object> getPage(){
    	Map<String, Object> page = new HashMap<>();
    	page.put("pageIndex", this.getPageIndex());
    	page.put("pageTotal", this.getPageTotal());
    	page.put("pageSize", this.getPageSize());
    	page.put("recordTotal", this.getRecordTotal());
    	return page;
    }

}
