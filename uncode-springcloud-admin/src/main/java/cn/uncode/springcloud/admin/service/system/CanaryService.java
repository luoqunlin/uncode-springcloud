package cn.uncode.springcloud.admin.service.system;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.uncode.dal.criteria.QueryCriteria;
import cn.uncode.springcloud.admin.dal.system.CanaryDAL;
import cn.uncode.springcloud.admin.model.system.dto.CanaryDTO;
import cn.uncode.springcloud.starter.web.result.P;

@Service
public class CanaryService {

    @Autowired 
    private CanaryDAL canaryDAL;
    

    public P<List<CanaryDTO>> getPageList(int pageIndex, int pageSize) {
    	QueryCriteria queryCriteria = new QueryCriteria();
    	if(pageIndex > 0) {
    		queryCriteria.setPageIndex(pageIndex);
    	}
    	if(pageSize > 0) {
    		queryCriteria.setPageSize(pageSize);
    	}
		Map<String, Object> resultMap = canaryDAL.getPageByCriteria4Map(queryCriteria);
		return P.valueOfDAL(resultMap, CanaryDTO.class);
    }
    
    public long add(CanaryDTO dto) {
    	Date time = new Date();
    	dto.setCreateTime(time);
    	dto.setUpdateTime(time);
        return (Long) canaryDAL.insertEntity(dto);
    }

    public int update(CanaryDTO dto) {
    	dto.setUpdateTime(new Date());
        return canaryDAL.updateEntityById(dto);
    }

    public int delete(List<Long> ids) {
    	QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.createCriteria().andColumnIn(CanaryDTO.ID, ids);
        return canaryDAL.deleteEntityByCriteria(queryCriteria);
    }

    public CanaryDTO getById(Long id) {
        return canaryDAL.getEntityById(id);
    }

    /**
     * 查询所有路由信息
     * @return
     */
    public List<CanaryDTO> getAll() {
    	QueryCriteria queryCriteria = new QueryCriteria();
		queryCriteria.setRecordIndex(0);
		queryCriteria.setLimit(1000);
		List<CanaryDTO> list = canaryDAL.getListByCriteria(queryCriteria);
        return list;
    }
    
}
